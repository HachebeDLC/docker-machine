#!/usr/bin/env bats

load "${BASE_TEST_DIR}/helpers.bash"

only_if_env DRIVER amazonec2

use_disposable_machine

mkdir -p "${MACHINE_STORAGE_PATH}"

export AWS_SSH_KEYPATH=${MACHINE_STORAGE_PATH}/id_rsa

@test "${DRIVER}: should create a fleet-based host" {
    teardown () {
        machine rm -f "${NAME}" || true
        aws ec2 delete-key-pair --key-name "${NAME}" || true
        aws ec2 delete-launch-template --launch-template-name "${NAME}" || true
        for i in seq 1 10; do aws ec2 delete-security-group --group-name "${NAME}" && break; sleep 10; done
    }

    default_vpc=$(aws ec2 describe-vpcs \
        --query 'Vpcs[?(IsDefault==`true`)].VpcId' \
        --output text \
    )
    default_subnet=$(aws ec2 describe-subnets \
        --filters "Name=vpc-id,Values=${default_vpc}" \
        --query 'Subnets[?(DefaultForAz==`true`)].SubnetId|[0]' \
        --output text \
    )

    group_id=$(aws ec2 create-security-group \
        --description "${NAME}" \
        --group-name "${NAME}" \
        --query 'GroupId' \
        --output text \
    )
    while ! aws ec2 describe-security-groups --group-name "${NAME}"; do sleep 1; done
    for port in 22 2376; do \
        aws ec2 authorize-security-group-ingress \
            --group-name "${NAME}" \
            --protocol tcp \
            --port "${port}" \
            --cidr 0.0.0.0/0
    done

    ssh-keygen -f "${AWS_SSH_KEYPATH}" -t rsa -N ''
    aws ec2 import-key-pair \
        --key-name "${NAME}" \
        --public-key-material "file://${AWS_SSH_KEYPATH}.pub"

    image_id=$(aws ssm get-parameters \
        --names /aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2 \
        --query 'Parameters[0].[Value]' \
        --output text \
    )

    aws ec2 create-launch-template --cli-input-json '{
        "LaunchTemplateName": "'"${NAME}"'",
        "LaunchTemplateData": {
            "ImageId": "ami-08d4ac5b634553e16",
            "InstanceType": "t2.micro",
            "KeyName": "'"${NAME}"'",
            "NetworkInterfaces": [{
                "DeviceIndex": 0,
                "SubnetId": "'"${default_subnet}"'",
                "Groups": ["'"${group_id}"'" ]
            }]
        }
    }'

    run machine create \
        --driver amazonec2 \
        --amazonec2-use-fleet \
        --amazonec2-launch-template "${NAME}" \
        --amazonec2-instance-type "t3a.micro" \
        --amazonec2-request-spot-instance \
        "${NAME}"

    echo ${output}
    [ "$status" -eq 0 ]

    instance_type=$(aws ec2 describe-instances \
        --filters Name=tag:Name,Values=$NAME \
        --query 'Reservations[0].Instances[0].InstanceType' \
        --output text \
    )
    [[ ${instance_type} = "t3a.micro" ]]

    instance_lifecycle=$(aws ec2 describe-instances \
        --filters Name=tag:Name,Values=$NAME \
        --query 'Reservations[0].Instances[0].InstanceLifecycle' \
        --output text \
    )
    [[ ${instance_lifecycle} = "spot" ]]
}
